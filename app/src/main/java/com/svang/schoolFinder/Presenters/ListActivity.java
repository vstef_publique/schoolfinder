package com.svang.schoolFinder.Presenters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.svang.schoolFinder.Adapters.SchoolAdapter;
import com.svang.schoolFinder.Interfaces.IGetEcolePrimaire;
import com.svang.schoolFinder.Interfaces.ISchoolListDelegate;
import com.svang.schoolFinder.Managers.DataManager;
import com.svang.schoolFinder.Models.EcolePrimaire;
import com.svang.schoolFinder.R;
import com.svang.schoolFinder.Utils.ActivityUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class ListActivity extends AppCompatActivity implements ISchoolListDelegate {

    //region Variables

    private String TAG = ListActivity.class.getSimpleName();

    private RecyclerView mSchoolList;
    private SwipeRefreshLayout mSwipeRefreshSchool;
    private RelativeLayout mEmptyViewRelativeLayout;
    private RelativeLayout mErrorViewRelativeLayout;
    private ProgressDialog mProgressDialog;

    private ArrayList<EcolePrimaire> mSchools;

    private Call<List<EcolePrimaire>> mGetPrimarySchoolCall;

    //endregion

    //region New Instance

    //  Permet de générer un intent d'appel de l'activité courante
    public static Intent newInstance(Context cxt) {
        return new Intent(cxt, ListActivity.class);
    }

    //endregion

    //region Default Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //  On ajoute un bouton de retour

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // On récupère tous les éléments graphiques

        mEmptyViewRelativeLayout = (RelativeLayout) findViewById(R.id.empty_empty_view_relative_layout);
        mErrorViewRelativeLayout = (RelativeLayout) findViewById(R.id.empty_error_view_relative_layout);

        mSwipeRefreshSchool = (SwipeRefreshLayout) findViewById(R.id.empty_school_refresh_refresh_layout);
        mSwipeRefreshSchool.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        mSchools = new ArrayList<>();
        mSchoolList = (RecyclerView) findViewById(R.id.empty_school_list_recycler_view);
        mSchoolList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        SchoolAdapter adapter = new SchoolAdapter();
        adapter.setDelegate(this);

        mSchoolList.setAdapter(adapter);

        //  Création et affichage de la progress dialog
        //  Fait patienter l'utilisateur pendant le chargement des données la première fois
        mProgressDialog = ActivityUtils.newProgressDialog(this,
                getString(R.string.app_name),
                getString(R.string.loading_schools));
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mGetPrimarySchoolCall != null) {
                    mGetPrimarySchoolCall.cancel();
                }
                dialog.dismiss();
                onBackPressed();
            }
        });
        mProgressDialog.show();

        //  On lance automatiquement la récupération des données
        refreshData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //  Dans le cas où l'utilisateur cliquerais sur la flèche retour '<-'
            //  On appel la méthode 'onBackPressed' pour conserver la logique de retour
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {

        super.onStop();

        //  Si la requête n'est pas null et qu'elle est en cours d'execution, on la stoppe.
        if (mGetPrimarySchoolCall != null) {
            mGetPrimarySchoolCall.cancel();
        }

    }

    //endregion

    //region Methods

    //  Permet de rafraîchir les données depuis le WebServices
    private void refreshData() {

        mEmptyViewRelativeLayout.setVisibility(View.GONE);
        mErrorViewRelativeLayout.setVisibility(View.GONE);

        mSwipeRefreshSchool.setEnabled(false);

        mGetPrimarySchoolCall = DataManager.getInstance().getPrimarySchool(new IGetEcolePrimaire() {
            @Override
            public void onSuccess(List<EcolePrimaire> schools) {
                mSchools = new ArrayList<EcolePrimaire>(schools);
                endRefresh(false);
            }
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getLocalizedMessage());
                mSchools = new ArrayList<EcolePrimaire>();
                endRefresh(true);
                ActivityUtils.showMessage(ListActivity.this,
                        getString(R.string.app_name),
                        getString(R.string.error_load_schools));
            }
        });

    }

    //  Fin du rafraîchissement
    //  On réagi en fonction de plusieurs critères :
    //      - La liste des écoles est vide
    //      - Une erreur est survenue
    //      - La liste des écoles possède des éléments
    private void endRefresh(boolean withError) {

        //  On cache la progress dialogue
        mProgressDialog.dismiss();

        mSwipeRefreshSchool.setRefreshing(false);
        mSwipeRefreshSchool.setEnabled(true);

        mSchoolList.getAdapter().notifyDataSetChanged();

        if (withError) {
            mErrorViewRelativeLayout.setVisibility(View.VISIBLE);
            return;
        }

        mErrorViewRelativeLayout.setVisibility(View.GONE);

        if (mSchools.size() == 0) {
            mEmptyViewRelativeLayout.setVisibility(View.VISIBLE);
        }

    }

    //endregion

    //region Implementation of School List Delegate

    @Override
    public int numberOfSchools() {
        return mSchools.size();
    }

    @Override
    public ArrayList<EcolePrimaire> getSchools() {
        return mSchools;
    }

    @Override
    public Activity getParentActity() {
        return this;
    }

    //endregion


}
