package com.svang.schoolFinder.Presenters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.svang.schoolFinder.Interfaces.IGetEcolePrimaire;
import com.svang.schoolFinder.Managers.DataManager;
import com.svang.schoolFinder.Models.EcolePrimaire;
import com.svang.schoolFinder.R;
import com.svang.schoolFinder.Utils.ActivityUtils;

import java.util.List;

import retrofit2.Call;

//  ATTENTION !! Ne fonctionne que si les Google Services sont installés sur le téléphone
public class MapsActivity extends ActionBarActivity implements OnMapReadyCallback {

    //region Variables

    private static final String SCHOOL_NAME = "SCHOOL_NAME";

    private GoogleMap mMap;
    private ProgressDialog mProgressDialog;
    private String mZoomToSchool = null;

    private Call<List<EcolePrimaire>> mGetPrimarySchool;

    //endregion

    //region New Instance

    //  Permet de générer un intent d'appel de l'activité courante
    public static Intent newInstance(Context cxt) {
        return new Intent(cxt, MapsActivity.class);
    }

    //  Permet de générer un intent d'appel de l'activité courante
    //  avec en paramètre l'école à cibler
    public static Intent newInstance(Context cxt, EcolePrimaire ecole) {
        Intent i = new Intent(cxt, MapsActivity.class);
        i.putExtra(SCHOOL_NAME, ecole.getNom());
        return i;
    }

    //endregion

    //region Default Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //  On ajoute un bouton de retour

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //  On intègre le fragment de la 'Map' dans l'activité courante
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    protected void onResume() {

        super.onResume();

        //  Récupération de l'intent d'ouverture
        //  2 cas possible :
        //      - On ouvre simplement la Map
        //      - On ouvre la Map en zoomant sur une école

        Intent i = getIntent();

        //  On met la valeur de 'SCHOOL_NAME' dans une variable locale
        //  ATTENTION ! Celle-ci peut être null !
        //  Il sera nécessaire de gérer lors de l'appel

        mZoomToSchool = i.getStringExtra(SCHOOL_NAME);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //  Dans le cas où l'utilisateur cliquerais sur la flèche retour '<-'
            //  On appel la méthode 'onBackPressed' pour conserver la logique de retour
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {

        super.onStop();

        //  On annule la tâche avant de quitter la page
        if (mGetPrimarySchool != null) {
            mGetPrimarySchool.cancel();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        //  On récupère la map généré
        mMap = googleMap;

        // Add a marker in Lyon and move the camera
        LatLng lyon = new LatLng(45.750000, 4.850000);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lyon, 10));

        //  Création et affichage de la progress dialog
        //  Fait patienter l'utilisateur pendant le chargement des données
        mProgressDialog = ActivityUtils.newProgressDialog(this,
                getString(R.string.app_name),
                getString(R.string.loading_schools));
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //  L'utilisateur à souhaiter annuler le chargement
                //  On annule la tâche Retrofit
                if (mGetPrimarySchool != null) {
                    mGetPrimarySchool.cancel();
                }
                //  On cache la progress dialog
                dialog.dismiss();
                //  On quitte la page
                onBackPressed();
            }
        });
        mProgressDialog.show();

        //  Récupération des écoles primaires via le WebServices
        mGetPrimarySchool = DataManager.getInstance().getPrimarySchool(new IGetEcolePrimaire() {
            @Override
            public void onSuccess(List<EcolePrimaire> schools) {

                //  On cache la progress dialogue
                mProgressDialog.dismiss();

                LatLng zoomSchool = null;

                //  Pour chacune des écoles on crée un Marker que l'on place sur la map
                for (EcolePrimaire school : schools) {

                    MarkerOptions options = new MarkerOptions();
                    options.position(school.getLatLong());
                    options.title(school.getNom());
                    options.snippet(school.getAddresse());

                    //  On modifie la couleur de l'icon en fonction du nombre d'élève
                    if (school.isOrange()) {
                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    }
                    else if (school.isGreen()) {
                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    }
                    else {
                        if (!school.isRed()) {
                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                        }
                    }

                    //  Si une école est en attente de zoom
                    //  On récupère ses coordonnées
                    if (mZoomToSchool != null && school.getNom().equals(mZoomToSchool)) {
                        zoomSchool = school.getLatLong();
                    }

                    //  On ajoute le marker à la carte
                    mMap.addMarker(options);

                }

                //  On zoom sur les coordonnées du marker en mémoire
                if (zoomSchool != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zoomSchool, 15));
                }

            }
            @Override
            public void onError(Throwable error) {

                //  Un problème est survenue !
                //  On cache la progress dialog
                mProgressDialog.dismiss();

                //  On affiche un message d'erreur
                ActivityUtils.showMessage(MapsActivity.this,
                        getString(R.string.app_name),
                        getString(R.string.error_load_schools));

            }
        });

    }

    //endregion


}
