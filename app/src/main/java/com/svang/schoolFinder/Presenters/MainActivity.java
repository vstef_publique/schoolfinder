package com.svang.schoolFinder.Presenters;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.svang.schoolFinder.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //region Variables

    private CardView mMapCardView;
    private CardView mListeCardView;

    //endregion

    //region Default Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  On récupère le bouton 'Map' et on lui attache un clickListener
        mMapCardView = (CardView) findViewById(R.id.main_maps_card_view);
        mMapCardView.setOnClickListener(this);

        //  On récupère le bouton 'Liste' et on lui attache un clickListener
        mListeCardView = (CardView) findViewById(R.id.main_list_card_view);
        mListeCardView.setOnClickListener(this);

    }

    //endregion

    //region On Click Listener Implementation

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_maps_card_view:
                Intent i = MapsActivity.newInstance(MainActivity.this);
                startActivity(i);
                break;
            case R.id.main_list_card_view:
                Intent j = ListActivity.newInstance(MainActivity.this);
                startActivity(j);
                break;
        }
    }

    //endregion

}
