package com.svang.schoolFinder.Models;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.svang.schoolFinder.R;

/**
 * Created by svang on 02/02/2018.
 */

public class EcolePrimaire {

    //region Variables

    private String nom;
    private String addresse;
    private int nbEleve;
    private int status;
    private String latitude;
    private String longitude;

    //endregion

    //region Get / Set

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public int getNbEleve() {
        return nbEleve;
    }

    public void setNbEleve(int nbEleve) {
        this.nbEleve = nbEleve;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    //endregion

    //region Methods

    //  Retourne TRUE dans le cas où l'école comporte PLUS de 300 élève
    public boolean isRed() {
        return getNbEleve() > 300;
    }

    //  Retourne TRUE dans le cas où l'école comporte ENTRE 150 et 300 élèves
    public boolean isOrange() {
        return getNbEleve() > 150 && nbEleve < 300;
    }

    //  Retourne TRUE dans le cas où l'école comporte MOINS de 150 élèves
    public boolean isGreen() {
        return getNbEleve() < 150;
    }

    //  Permet de générer un objet 'LatLng' en fonction de la latitude et de la longitude
    //  de l'école
    public LatLng getLatLong() {
        double lat = Double.parseDouble(getLatitude());
        double longi = Double.parseDouble(getLongitude());
        return new LatLng(lat, longi);
    }

    //  Permet de récupérer le libellé à afficher suivant le status
    public String getShowableStatus(Context cxt) {
        if (getStatus() == 1) {
            return cxt.getString(R.string.common_open);
        }
        return cxt.getString(R.string.common_close);
    }

    //endregion

}
