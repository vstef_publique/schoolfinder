package com.svang.schoolFinder.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.svang.schoolFinder.Presenters.MapsActivity;
import com.svang.schoolFinder.Models.EcolePrimaire;
import com.svang.schoolFinder.R;

/**
 * Created by svang on 02/02/2018.
 */

public class SchoolViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;

    private RelativeLayout mBodyRelativeLayout;
    private TextView mNameTextView;
    private TextView mAddressTextView;
    private TextView mStatusTextView;
    private TextView mNbEleveTextView;
    private TextView mLatitudeTextView;
    private TextView mLongitudeTextView;

    public SchoolViewHolder(View itemView) {

        super(itemView);

        mBodyRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.cell_school_body);
        mNameTextView = (TextView) itemView.findViewById(R.id.cell_school_name);
        mAddressTextView = (TextView) itemView.findViewById(R.id.cell_school_address);
        mStatusTextView = (TextView) itemView.findViewById(R.id.cell_school_status);
        mNbEleveTextView = (TextView) itemView.findViewById(R.id.cell_school_nb_eleve);
        mLatitudeTextView = (TextView) itemView.findViewById(R.id.cell_school_latitude);
        mLongitudeTextView = (TextView) itemView.findViewById(R.id.cell_school_longitude);

    }

    //  Permet de setter le context courant au ViewHolder
    public void setContext(Context cxt) {
        this.mContext = cxt;
    }

    //  Permet de customiser la vue en fonction de l'école passé
    //  en paramètre
    public void layoutForSchool(final EcolePrimaire ecole) {

        if (ecole != null && mContext != null) {

            //  On set les différents textes
            setTextTo(mNameTextView, ecole.getNom());
            setTextTo(mAddressTextView, ecole.getAddresse());
            setTextTo(mStatusTextView, "Status : " + ecole.getShowableStatus(mContext));
            setTextTo(mNbEleveTextView, "Nombre d'élève : " + ecole.getNbEleve());
            setTextTo(mLatitudeTextView, "Latitude : " + ecole.getLatitude());
            setTextTo(mLongitudeTextView, "Longitude : " + ecole.getLongitude());

            //  On test le nombre d'élève et on set un couleur
            //  en fonction du résultat
            if (ecole.isGreen()) {
                mBodyRelativeLayout.setBackgroundResource(android.R.color.holo_green_light);
            }
            else if (ecole.isOrange()) {
                mBodyRelativeLayout.setBackgroundResource(android.R.color.holo_orange_light);
            }
            else if (ecole.isRed()) {
                mBodyRelativeLayout.setBackgroundResource(android.R.color.holo_red_light);
            }
            else {
                mBodyRelativeLayout.setBackgroundResource(android.R.color.holo_purple);
            }

            //  On récupère le clique de la vue
            mBodyRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mContext != null) {
                        //  Si le context n'est pas null, on récupère l'intent d'ouvrerture de la page
                        //  Map et on l'ouvre avec en paramètre l'école sélectionné
                        Intent i = MapsActivity.newInstance(mContext, ecole);
                        mContext.startActivity(i);
                    }
                }
            });

        }

    }

    //  Permet de setter un texte à un TextView en toute sécurité
    //  (gestion du null)
    private void setTextTo(TextView tv, String value) {
        if (tv != null) {
            tv.setText(value);
        }
    }

}
