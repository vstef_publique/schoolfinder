package com.svang.schoolFinder.Widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by svang on 02/02/2018.
 */

//  Permet d'avoir un RelativeLayout ayant une hauteur égale à sa largeur.
public class SquareRelativeLayout extends RelativeLayout {

    //region Constructor

    public SquareRelativeLayout(Context context) {
        super(context);
    }

    public SquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //endregion

    //region Default Methods

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //  On surcharge la méthode 'onMeasure' pour obligé
        //  la vue à avoir la hauteur égale à sa largeur
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    //endregion
}
