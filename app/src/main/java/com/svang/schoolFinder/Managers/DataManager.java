package com.svang.schoolFinder.Managers;

import com.svang.schoolFinder.Interfaces.IGetEcolePrimaire;
import com.svang.schoolFinder.Models.EcolePrimaire;
import com.svang.schoolFinder.Services.APIService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by svang on 02/02/2018.
 *
 * Le DataManager à vocation à gérer les données.
 */
public class DataManager {

    //region Variables

    private APIService mApiService;

    public static final String HOST = "put your host here";
    public static final String PORT = "80";

    public static final String ENDPOINT = "http://" + HOST + ":" + PORT + "/";

    //endregion

    //region Constructor

    private DataManager() {

        //  Création du service API
        mApiService = new Retrofit.Builder()
            .baseUrl(ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(APIService.class);

    }

    //endregion

    //region Singleton

    private static DataManager INSTANCE = null;

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            //  Création de l'instance
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    //endregion

    //region Methods

    public Call<List<EcolePrimaire>> getPrimarySchool(final IGetEcolePrimaire callback) {

        Call<List<EcolePrimaire>> call = mApiService.getEcolePrimaire();

        //  Ajout à la liste d'execution de Retrofit
        call.enqueue(new Callback<List<EcolePrimaire>>() {
            @Override
            public void onResponse(Call<List<EcolePrimaire>> call, Response<List<EcolePrimaire>> response) {
                if (response == null || response.body() == null) {
                    if (callback != null) {
                        callback.onSuccess(new ArrayList<EcolePrimaire>());
                    }
                    return;
                }
                if (callback != null) {
                    callback.onSuccess(new ArrayList<EcolePrimaire>(response.body()));
                }
            }
            @Override
            public void onFailure(Call<List<EcolePrimaire>> call, Throwable t) {
                if (callback != null) {
                    if (!call.isCanceled()) {
                        callback.onError(t);
                    }
                }
            }
        });

        return call;

    }

    //endregion

}
