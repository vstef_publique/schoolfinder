package com.svang.schoolFinder.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.svang.schoolFinder.Interfaces.ISchoolListDelegate;
import com.svang.schoolFinder.Models.EcolePrimaire;
import com.svang.schoolFinder.R;
import com.svang.schoolFinder.ViewHolders.SchoolViewHolder;

/**
 * Created by svang on 02/02/2018.
 */

public class SchoolAdapter extends RecyclerView.Adapter {

    private ISchoolListDelegate mDelegate;

    public void setDelegate(ISchoolListDelegate delegate) {
        mDelegate = delegate;
    }

    @Override
    public int getItemCount() {
        if (mDelegate == null) {
            return 0;
        }
        return mDelegate.numberOfSchools();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //  Dans le cas présent, on a uniquement 1 type de cellules
        //  par conséquent on génère toujours la même vue avec
        //  le même ViewHolder

        if (mDelegate == null) {
            return null;
        }

        View v = mDelegate.getParentActity().getLayoutInflater().inflate(R.layout.cell_school, null);
        return new SchoolViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (mDelegate == null) {
            return;
        }

        //  On récupère l'école de la position courante
        EcolePrimaire school = mDelegate.getSchools().get(position);

        //  Si elle n'est pas vide et que le viewHolder est bien celui d'une SchoolViewHolder
        //  alors on cast le viewHolder
        if (school != null && holder instanceof SchoolViewHolder) {

            //  On cast le viewHolder
            SchoolViewHolder schoolViewHolder = (SchoolViewHolder) holder;

            //  On set le context (nécessaire pour le click)
            schoolViewHolder.setContext(mDelegate.getParentActity());

            //  On apsse au viewHolder l'école courante pour customiser la vuecvg
            schoolViewHolder.layoutForSchool(school);

        }

    }

}