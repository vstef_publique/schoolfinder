package com.svang.schoolFinder.Services;

import com.svang.schoolFinder.Models.EcolePrimaire;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by svang on 02/02/2018.
 */

public interface APIService {

    @GET("getEcolePrimaire")
    Call<List<EcolePrimaire>> getEcolePrimaire();

}
