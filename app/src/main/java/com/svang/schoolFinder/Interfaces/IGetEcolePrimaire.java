package com.svang.schoolFinder.Interfaces;

import com.svang.schoolFinder.Models.EcolePrimaire;

import java.util.List;

/**
 * Created by svang on 02/02/2018.
 */

public interface IGetEcolePrimaire {
    void onSuccess(List<EcolePrimaire> schools);
    void onError(Throwable error);
}
