package com.svang.schoolFinder.Interfaces;

import android.app.Activity;

import com.svang.schoolFinder.Models.EcolePrimaire;

import java.util.ArrayList;

/**
 * Created by svang on 02/02/2018.
 */

public interface ISchoolListDelegate {
    int numberOfSchools();
    ArrayList<EcolePrimaire> getSchools();
    Activity getParentActity();
}
