package com.svang.schoolFinder.Utils;

/**
 * Created by svang on 02/02/2018.
 */

public class StringUtils {

    //  Permet de vérifier qu'une valeur est NULL ou EMPTY
    public static boolean isNullOrEmpty(String value) {
        return value == null || value.length() == 0;
    }

}
