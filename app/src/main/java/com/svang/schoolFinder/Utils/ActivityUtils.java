package com.svang.schoolFinder.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by svang on 02/02/2018.
 */

public class ActivityUtils {

    //   Permet d'afficher un boite de dialogue de type 'Modal' directement
    //   dans le contexte passé en paramètre
    public static void showMessage(Context cxt, String title, String message) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(cxt);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(cxt.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    //  Permet d'afficher un toast dans le contexte passé en paramètre
    public static void showToast(Context cxt, String title) {
        Toast.makeText(cxt, title, Toast.LENGTH_LONG).show();
    }

    //  Permet d'avoir une instance de ProgressDialog pré-paramétré
    public static ProgressDialog newProgressDialog(Context cxt, String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(cxt);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

}
